package com.qa.testNf.BaseTest;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


import org.testng.annotations.AfterTest;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.qa.config.properties.prop;
import com.qa.testdata.InPutController;


import io.github.bonigarcia.wdm.WebDriverManager;


public class BaseTest {

	public static WebDriver driver;

	@BeforeTest
	@Parameters("browser")  //(dataProvider="testdata")
	public void BrowserSetup(String browser) throws IOException {
		
		//Chrome Driver initialize
		if(browser.equalsIgnoreCase("Chrome")) {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		prop.fetchurl("url");
		driver.manage().timeouts().implicitlyWait(Duration.ofMinutes(1));
		}
		
		//Edge Driver initialize
		else if(browser.equalsIgnoreCase("Edge")){
			WebDriverManager.edgedriver().setup();
			driver = new EdgeDriver();
			driver.manage().window().maximize();
			prop.fetchurl("url");
			driver.manage().timeouts().implicitlyWait(Duration.ofMinutes(1));
			
		}
		
		//Firefox Driver initialize
		else if(browser.equalsIgnoreCase("Firefox")){
			WebDriverManager.firefoxdriver().setup();;
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			prop.fetchurl("url");
			driver.manage().timeouts().implicitlyWait(Duration.ofMinutes(1));
		
	}
	}

	@AfterTest
	public void tearDown() {
		driver.close();
	}
	@DataProvider(name="TestData")
	public String[] hrm() throws IOException{
	 String[] data=InPutController.getData();
	return data;
	 }

	}

