package com.testNF.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.ActionDriver.MoveToeEements;
import com.qa.pageElement.Homepage;
import com.qa.testNf.BaseTest.BaseTest;
import com.qa.utility.ReusableMethod;

public class HomePageTest extends BaseTest {

	Homepage hp;

	@BeforeClass
	public void initObject() {
		hp = new Homepage();

	}

	@Test(priority = 1)
	public void HoverMouseOnHeaderMenu() throws InterruptedException {
		MoveToeEements.MoveToMultipleElement(hp.headerMenu);
	}

	@Test(priority = 2)
	public void hovermouseonSunbHeaderMenu() throws InterruptedException {
		//MoveToeEements.MoveToSingleElement(hp.grocery);
		MoveToeEements.MoveToMultipleElement(hp.subheaderMenu);
	}

	@Test(dataProvider = "TestData",priority=3)
	public void login(String value) throws InterruptedException {
		hp.searchbar.click();
		hp.searchbar.sendKeys(value);
		Thread.sleep(2000);
		hp.searchbar.clear();
		
		
	}

	

}
