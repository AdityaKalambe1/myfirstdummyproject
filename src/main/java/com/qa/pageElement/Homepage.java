package com.qa.pageElement;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.testNf.BaseTest.BaseTest;



public class Homepage extends BaseTest {
	@FindBy(xpath="//*[@class=\"o-menu\"]")
	public List< WebElement>headerMenu;
	
	@FindBy(xpath="//*[contains(@id,\"nav_link\")]")
	public List< WebElement>subheaderMenu;
	
	@FindBy(xpath="onclick=\"openNav()\"")
	public WebElement menubar;
	
	@FindBy(xpath="//*[@id=\"autocomplete-0-input\"]")
	public  WebElement searchbar;
	
	@FindBy(xpath="//*[@class=\"logged\"]/a")
    public WebElement signInUp;	
	
	@FindBy(xpath="//*[@class=\"cart_text\"]")
	public WebElement addToCart;	
	
	@FindBy(xpath="//*[@class=\"delivery_content\"]")
	public WebElement delivercode;	
	
	@FindBy(xpath="//*[@id=\"nav_link_2\"]")
	public WebElement grocery;
	
	@FindBy(xpath="//*[@id=\"nav_link_6047\"]")
	public WebElement premiumFruits;
	
	@FindBy(xpath="//*[@id=\"nav_link_1687\"]")
	public WebElement homeAndKitchen;
	
	@FindBy(xpath="//*[@id=\"nav_link_3\"]")
	public WebElement fashion;
	
	
	@FindBy(xpath="//*[@id=\"nav_link_4\"]")
	public WebElement electronics;	
	
	@FindBy(xpath="//*[@id=\"nav_link_5\"]")
	public WebElement beauty;	
	
	@FindBy(xpath="//*[@id=\"nav_link_1524\"]")
	public WebElement jwellery;
	
	
	
	public Homepage() {
		PageFactory.initElements(driver, this);
	}
	

}