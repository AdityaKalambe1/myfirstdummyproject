package com.qa.testdata;

import java.io.File;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;

import org.testng.annotations.BeforeClass;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
/*
 * InputEcel class are used to provide the data to test case at runtime
  to validate test cases with multiple data
 *This excel code is handled by using Apache POI jars
 */

public class InPutController {
	
	@DataProvider(name = "TestData")
	public static String[] getData() throws IOException

	{
		String path = "E:\\eclipse workspace 2\\JioMart.com\\Login.xlsx";
		File file = new File(path);
		FileInputStream fs = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fs);
		XSSFSheet sheet = wb.getSheetAt(0);
		int row = sheet.getPhysicalNumberOfRows();
		System.out.println(row);
		String[] data = new String[row];
		for (int i = 0; i < row; i++) {
			String data1 = sheet.getRow(i).getCell(0).getStringCellValue();
			data[i] = data1;
		}
		return data;
	}
	

}
